//
//  GifModel.swift
//  Gipsy
//
//  Created by Anaaya Nayanesh Acharya on 24/06/17.
//  Copyright © 2017 Ashwini Acharya. All rights reserved.
//

import UIKit
import SwiftyJSON


class GifModel{
    
    var url: String?
    var id: String?
    
    convenience init(data: JSON) {
        self.init()
        if let gifId = data["id"].string {
            id = gifId
        }
        if let gifURL = data["images"][Constants.preferredImageType]["url"].string {
            url = gifURL
        }
    }
    
}

