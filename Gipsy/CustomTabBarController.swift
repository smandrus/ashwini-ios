//
//  CustomTabBarController.swift
//  Gipsy
//
//  Created by Anaaya Nayanesh Acharya on 23/06/17.
//  Copyright © 2017 Ashwini Acharya. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController
{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up the View Controllers
        
        //1. Trending View Controller : Contains UITableViewController with SearchBar
        
        let trendingViewController = FeedViewController()
        trendingViewController.title = "GiFeed"
        let trendingNavigationController = UINavigationController.init(rootViewController:trendingViewController)
        trendingNavigationController.title = "Feed"
        trendingNavigationController.tabBarItem.image = UIImage.init(named: "Search")
        
        //2. Favourite View Controller : Contains UICollectionView
        
        let favouriteViewController = FavouriteViewController.init(collectionViewLayout: UICollectionViewFlowLayout())
        favouriteViewController.title = "Favourites"
        let favouriteNavigationController = UINavigationController.init(rootViewController: favouriteViewController)
        favouriteNavigationController.title = "Favourites"
        favouriteNavigationController.tabBarItem.image = UIImage.init(named: "Favour")
        
        
        viewControllers = [trendingNavigationController,favouriteNavigationController]
        
        // TabBar Properties
        
        tabBar.isTranslucent = false
        
    }
}
