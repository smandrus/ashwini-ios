//
//  ViewController.swift
//  Gipsy
//
//  Created by Anaaya Nayanesh Acharya on 23/06/17.
//  Copyright © 2017 Ashwini Acharya. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class FeedViewController: UITableViewController,UISearchBarDelegate {
    
    static let cellId = "CellIdentifier"
    
    let searchController = UISearchController(searchResultsController: nil)
    
    fileprivate var gifFeed = GifFeedModel(type: .trending)
    fileprivate var searchBar: UISearchBar!
    fileprivate var loaded: Bool = false
    fileprivate var reachability: NetworkReachabilityManager!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundColor = UIColor.rgb(242, green: 242, blue: 242)
        tableView.register(FeedViewCell.self, forCellReuseIdentifier: FeedViewController.cellId)
        
        
        // Add UISearchController to the TableView
        tableView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        searchController.searchBar.barTintColor = UIColor.rgb(51, green: 90, blue: 149)
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.delegate = self
        
        // refresh control
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshFeed), for: .valueChanged)
        self.refreshControl = refreshControl
        
        
        // reachability
        reachability = NetworkReachabilityManager()
        reachability.startListening()
        reachability.listener = { status -> Void in
            switch status {
            case .notReachable:
                refreshControl.endRefreshing()
                let alert = self.alertControllerWithMessage(Constants.checkInternetConnectionMessage)
                self.present(alert, animated: true, completion: nil)
                
        
            case .reachable(.ethernetOrWiFi), .reachable(.wwan):
                (self.loaded == false) ? self.loadFeed() : self.loadMoreFeed()
            default: break
            }
        }
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl?.endRefreshing()
        
    }
    
    
    func refreshFeed() {
        gifFeed.clearFeed()
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        loadMoreFeed()
    }

    
    
    func loadFeed() {
        gifFeed.requestFeed(20, offset: 0, rating: nil, terms: nil, comletionHandler: { (succeed, _, error) -> Void in
            if succeed {
                self.loaded = true
                self.tableView.reloadData()
                self.loadMoreFeed()
            } else if let error = error {
                let alert = self.alertControllerWithMessage(error)
                self.present(alert, animated: true, completion: nil)
                
            }
        })
    }
    
    func loadMoreFeed() {
        gifFeed.requestFeed(20, offset: gifFeed.currentOffset, rating: nil, terms: nil, comletionHandler: { (succeed, total, error) -> Void in
            if succeed, let total = total {
                var indexPaths = [IndexPath]()
                for i in (self.gifFeed.currentOffset - total)..<self.gifFeed.currentOffset {
                    let indexPath = IndexPath.init(item: i, section: 0)
                    indexPaths.append(indexPath)
                }
                
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: indexPaths, with: UITableViewRowAnimation.bottom)
                self.tableView.endUpdates()
            } else if let error = error {
                let alert = self.alertControllerWithMessage(error)
                self.present(alert, animated: true, completion: nil)
    
            }
        })
    }
    
    // Favourite/Unfavourite Gifs
    func addFavourite(_ sender: AnyObject) {
        
        let button = sender as? UIButton
        let cell = button?.superview as? FeedViewCell
        if (CoreDataManager.sharedInstance.fetchFavouriteGifIds()?.contains((cell?.gif?.id)!))!{
            let deleteSuccess = CoreDataManager.sharedInstance.deleteFavouriteGif(gifId: (cell?.gif?.id)!)
            button?.setImage(UIImage(named:(deleteSuccess == true) ? Constants.unfavouriteIcon : Constants.favouriteIcon), for:.normal)
        } else {
            let insertSuccess = CoreDataManager.sharedInstance.insertFavouriteGif(gifId: (cell?.gif?.id)!, gifUrl: (cell?.gif?.url)!)
            button?.setImage(UIImage(named:(insertSuccess == true) ? Constants.favouriteIcon : Constants.unfavouriteIcon), for:.normal)
        }
    }
    

    
    
    // MARK: UIScrollView Delegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tableView.bounds.intersects(CGRect(x: 0, y: tableView.contentSize.height - Constants.screenHeight / 2, width: tableView.frame.width, height: Constants.screenHeight / 2)) && tableView.contentSize.height > 0 && reachability.isReachable {
            loadMoreFeed()
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if gifFeed.gifsArray.count == 0{
            TableViewHelper.EmptyMessage(message: Constants.checkInternetConnectionMessage, viewController: self)
            return 0
        } else {
            self.tableView?.backgroundView = nil;
            return gifFeed.gifsArray.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedViewController.cellId, for: indexPath) as! FeedViewCell
        
        cell.gif = gifFeed.gifsArray[indexPath.row]
        cell.cellButton.setImage(UIImage(named:((CoreDataManager.sharedInstance.fetchFavouriteGifIds()?.contains((cell.gif?.id)!))! ? Constants.favouriteIcon : Constants.unfavouriteIcon)), for:.normal)
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.rowHeight
    }

    
    // MARK: UISearchBar Delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !reachability.isReachable {
            let alert = alertControllerWithMessage(Constants.checkInternetConnectionMessage)
            self.present(alert, animated: true, completion: nil)
            return
        }
        if let searchTerms = searchBar.text, searchTerms != "" {
            let result = GifSearchViewController()
            result.searchTerms = searchTerms
            self.navigationController?.pushViewController(result, animated: true)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        view.endEditing(true)
        searchBar.showsCancelButton = false
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        return true
    }


}





