//
//  GifSearchViewController.swift
//  Gipsy
//
//  Created by Anaaya Nayanesh Acharya on 02/07/17.
//  Copyright © 2017 Ashwini Acharya. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class GifSearchViewController: UITableViewController {
    
    static let cellId = "CellIdentifier"
    
    var searchTerms: String!
    fileprivate var gifFeed = GifFeedModel(type: .search)
    fileprivate var loaded: Bool = false
    fileprivate var reachability: NetworkReachabilityManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor.rgb(242, green: 242, blue: 242)
        self.tableView.register(FeedViewCell.self, forCellReuseIdentifier: GifSearchViewController.cellId)
        
        // Search Functionality
        
        if searchTerms == nil {
            searchTerms = ""
        }
        if searchTerms.characters.count > 15 {
            self.title = searchTerms.substring(with: Range<String.Index>(searchTerms.index(searchTerms.startIndex, offsetBy: 0)..<searchTerms.index(searchTerms.startIndex, offsetBy: 14))) + "..."
        } else {
            self.title = searchTerms
        }

       
        // Reachability
            
        reachability = NetworkReachabilityManager()
        reachability.startListening()
        reachability.listener = { status -> Void in
            switch status {
            case .notReachable:
                let alert = self.alertControllerWithMessage(Constants.checkInternetConnectionMessage)
                self.present(alert, animated: true, completion: nil)
            case .reachable(.ethernetOrWiFi), .reachable(.wwan):
                (self.loaded == false) ? self.loadFeed() : self.loadMoreFeed()
            default: break
            }
        }
        
        
        
    }
    
    // MARK: Feeds
    
    func loadFeed() {
        gifFeed.requestFeed(20, offset: 0, rating: nil, terms: searchTerms, comletionHandler: { (succeed, _, error) -> Void in
            if succeed {
                self.loaded = true
                self.tableView.reloadData()
                self.loadMoreFeed()
            } else if let error = error {
                let alert = self.alertControllerWithMessage(error)
                self.present(alert, animated: true, completion: nil)
                
            }
        })
    }
    
    
    func loadMoreFeed() {
        gifFeed.requestFeed(20, offset: gifFeed.currentOffset, rating: nil, terms: searchTerms, comletionHandler: { (succeed, total, error) -> Void in
            if succeed, let total = total {
                var indexPaths = [IndexPath]()
                for i in (self.gifFeed.currentOffset - total)..<self.gifFeed.currentOffset {
                    let indexPath = IndexPath.init(item: i, section: 0)
                    indexPaths.append(indexPath)
                }
                
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: indexPaths, with: UITableViewRowAnimation.bottom)
                self.tableView.endUpdates()
            } else if let error = error {
                let alert = self.alertControllerWithMessage(error)
                self.present(alert, animated: true, completion: nil)
                
            }
        })
    }
   
    // Favourite/Unfavourite Gifs
    func addFavourite(_ sender: AnyObject) {
        
        let button = sender as? UIButton
        let cell = button?.superview as? FeedViewCell
        
        if (CoreDataManager.sharedInstance.fetchFavouriteGifIds()?.contains((cell?.gif?.id)!))!{
            let deleteSuccess = CoreDataManager.sharedInstance.deleteFavouriteGif(gifId: (cell?.gif?.id)!)
            button?.setImage(UIImage(named:(deleteSuccess == true) ? Constants.unfavouriteIcon : Constants.favouriteIcon), for:.normal)
        } else {
            let insertSuccess = CoreDataManager.sharedInstance.insertFavouriteGif(gifId: (cell?.gif?.id)!, gifUrl: (cell?.gif?.url)!)
            button?.setImage(UIImage(named:(insertSuccess == true) ? Constants.favouriteIcon : Constants.unfavouriteIcon), for:.normal)
        }
    }
    
    
       // MARK: UIScrollView Delegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tableView.bounds.intersects(CGRect(x: 0, y: tableView.contentSize.height - Constants.screenHeight / 2, width: tableView.frame.width, height: Constants.screenHeight / 2)) && tableView.contentSize.height > 0 && reachability.isReachable {
            loadMoreFeed()
        }
    }
    
    
    // MARK: UITableView Datasource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gifFeed.gifsArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: GifSearchViewController.cellId, for: indexPath) as! FeedViewCell
        
        cell.gif = gifFeed.gifsArray[indexPath.row]
        cell.cellButton.setImage(UIImage(named:((CoreDataManager.sharedInstance.fetchFavouriteGifIds()?.contains((cell.gif?.id)!))! ? Constants.favouriteIcon : Constants.unfavouriteIcon)), for:.normal)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.rowHeight
    }
    
    
    
}



