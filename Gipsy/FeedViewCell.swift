//
//  FeedViewCell.swift
//  Gipsy
//
//  Created by Anaaya Nayanesh Acharya on 24/06/17.
//  Copyright © 2017 Ashwini Acharya. All rights reserved.
//


import UIKit
import FLAnimatedImage
import SDWebImage

// Set up Custom TableViewCell

class FeedViewCell: UITableViewCell
{
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViewForCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let cellImageView: FLAnimatedImageView! = {
        let imageView = FLAnimatedImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    var cellButton: UIButton! = {
        let button = UIButton()
        button.layer.cornerRadius = 20
        button.contentMode = .scaleAspectFill
        button.addTarget(self, action: #selector(FeedViewController.addFavourite(_:)), for:.touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var gif: GifModel? {
        didSet {
            if let gif = gif, let url = gif.url {
                cellImageView.sd_setImage(with: URL.init(string: url))
                cellImageView.sd_setImage(with: URL.init(string: url), placeholderImage: UIImage.init(named: Constants.placeholderImage))
            }
        }
    }
    
    func setUpViewForCell()
    {
        
        self.addSubview(cellImageView)
        self.addSubview(cellButton)
        
        // Set ImageView Constraints
        cellImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        cellImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        cellImageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        cellImageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        // Set Button Constraints
        cellButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        cellButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        cellButton.bottomAnchor.constraint(equalTo: cellImageView.bottomAnchor, constant: -10).isActive = true
        cellButton.rightAnchor.constraint(equalTo: cellImageView.rightAnchor, constant: -10).isActive = true
    }
    
}
