//
//  Constants.swift
//  Gipsy
//
//  Created by Anaaya Nayanesh Acharya on 27/06/17.
//  Copyright © 2017 Ashwini Acharya. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let screenHeight: CGFloat = UIScreen.main.bounds.height
    static let rowHeight: CGFloat = 200
    
    static let searchResultsLimit: Int = 250
    static let preferredImageType = "fixed_height_downsampled"
    
    static let favouriteIcon = "fav"
    static let unfavouriteIcon = "unfav"
    static let placeholderImage = "placeholder_image"
    
    
    static let checkInternetConnectionMessage = "Please make sure you are connected to the internet and try again."
    static let emptyFavouriteListMessage = "You don't have any favorites yet. All your favorites will show up here. You can add favorites from the Gif Feed Tab."
    
}
