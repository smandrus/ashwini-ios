//
//  CoreDataManager.swift
//  Gipsy
//
//  Created by Anaaya Nayanesh Acharya on 03/07/17.
//  Copyright © 2017 Ashwini Acharya. All rights reserved.
//

import UIKit
import CoreData


class CoreDataManager: NSObject {
    static let sharedInstance = CoreDataManager()
    
    
    func getAppDelegate()->AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func getContext() -> NSManagedObjectContext {
        return getAppDelegate().persistentContainer.viewContext
    }
    
    func fetchFavouriteGifIds() -> [String]?{
        
        var favGifIds = [String]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "FavouriteGifs")
        fetchRequest.propertiesToFetch = ["gifId"]
        if let favouriteGifIds = try! getContext().fetch(fetchRequest) as? [FavouriteGifs]{
            for favouriteGifId in favouriteGifIds  {
                favGifIds.append(favouriteGifId.gifId!)
            }
            
        }
        return favGifIds
    }
    
    
    func fetchFavouriteGifs() -> [NSManagedObject]? {
        let request = NSFetchRequest<NSFetchRequestResult>.init(entityName: "FavouriteGifs")
        request.returnsObjectsAsFaults = false
        var results:[NSManagedObject]? = nil
        do {
            results = try getContext().fetch(request) as? [NSManagedObject]
        } catch {
            print("Fetching Failed")
        }
        return results
        
    }

    
    func insertFavouriteGif(gifId: String, gifUrl: String) -> Bool {
        let favouriteGif = NSEntityDescription.insertNewObject(forEntityName: "FavouriteGifs", into: getContext())
        favouriteGif.setValue(gifId, forKey: "gifId")
        favouriteGif.setValue(gifUrl, forKey: "gifUrl")
        do{
            try getContext().save()
            return true
        } catch {
            return false
        }
    }
    func deleteFavouriteGif(gifId: String) -> Bool{
        let fetchRequest = NSFetchRequest<FavouriteGifs>.init(entityName: "FavouriteGifs")
        fetchRequest.predicate = NSPredicate.init(format: "gifId == %@",gifId)
        let result = try? getContext().fetch(fetchRequest)
        let resultData = result!
        for object in resultData {
            getContext().delete(object)
        }
        do{
            try getContext().save()
            return true
         
        } catch {
            return false
        }
    }
}
