//
//  Extensions.swift
//  Gipsy
//
//  Created by Anaaya Nayanesh Acharya on 24/06/17.
//  Copyright © 2017 Ashwini Acharya. All rights reserved.
//

import UIKit

// UIColor Extension: custom colors with RGB values

extension UIColor {
    
    static func rgb(_ red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
}

extension UIViewController {
    
       
    // MARK: AlertController
    func alertControllerWithMessage(_ message: String) -> UIAlertController {
        
        let alertController = UIAlertController.init(title: "Gipsy", message: message, preferredStyle: .alert)
        let confirm = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(confirm)
        return alertController
        
    }
    
    
}

