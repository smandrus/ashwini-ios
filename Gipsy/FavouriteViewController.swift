//
//  FavouriteViewController.swift
//  Gipsy
//
//  Created by Anaaya Nayanesh Acharya on 23/06/17.
//  Copyright © 2017 Ashwini Acharya. All rights reserved.
//

import UIKit
import CoreData


class FavouriteViewController: UICollectionViewController,UICollectionViewDelegateFlowLayout
{
    var favGifsArray = [GifModel]()
    let cellId = "CellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.backgroundColor = UIColor.rgb(242, green: 242, blue: 242)
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(FavouriteViewCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.retrieveFavouriteGifsAndReload()
    }    
    
    func retrieveFavouriteGifsAndReload() {
        favGifsArray = []
        if let fetchedData:[NSManagedObject] = CoreDataManager.sharedInstance.fetchFavouriteGifs(){
            if fetchedData.count > 0{
                for result in fetchedData{
                    let favGifModel = GifModel()
                    favGifModel.id = result.value(forKey: "gifId") as? String
                    favGifModel.url = result.value(forKey: "gifUrl") as? String
                    self.favGifsArray.append(favGifModel)
                }
            } else {
                print("No results")
            }
        }
        
        collectionView?.reloadData()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if favGifsArray.count == 0{
            
            CollectionViewHelper.EmptyMessage(message: Constants.emptyFavouriteListMessage, viewController: self)
            return 0
            
        } else {
            self.collectionView?.backgroundView = nil;
            return favGifsArray.count
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! FavouriteViewCell

        cell.gif = favGifsArray[indexPath.row]
        cell.cellButton.setImage(UIImage(named:((CoreDataManager.sharedInstance.fetchFavouriteGifIds()?.contains((cell.gif?.id)!))! ? Constants.favouriteIcon : Constants.unfavouriteIcon)), for:.normal)
         return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:view.frame.width, height: Constants.rowHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // Unfavourite Gifs
    func deleteFavourite(_ sender: AnyObject) {
        
        let button = sender as? UIButton
        let cell = button?.superview as? FavouriteViewCell
        if (CoreDataManager.sharedInstance.fetchFavouriteGifIds()?.contains((cell?.gif?.id)!))!{
            let deleteSuccess = CoreDataManager.sharedInstance.deleteFavouriteGif(gifId: (cell?.gif?.id)!)
            button?.setImage(UIImage(named:(deleteSuccess == true) ? Constants.unfavouriteIcon : Constants.favouriteIcon), for:.normal)
            self.retrieveFavouriteGifsAndReload()
        }
    }
}

