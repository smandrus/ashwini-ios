//
//  CollectionViewHelper.swift
//  Gipsy
//
//  Created by Anaaya Nayanesh Acharya on 04/07/17.
//  Copyright © 2017 Ashwini Acharya. All rights reserved.
//

import UIKit

class CollectionViewHelper {
    
    class func EmptyMessage(message:String, viewController:UICollectionViewController) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: viewController.view.bounds.size.width, height: viewController.view.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        viewController.collectionView?.backgroundView = messageLabel;
    }
}
